﻿# Host: localhost  (Version 5.7.24)
# Date: 2020-02-27 15:43:10
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "data_rows"
#

DROP TABLE IF EXISTS `data_rows`;
CREATE TABLE `data_rows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data_type_id` int(10) unsigned NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`),
  CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "data_rows"
#

INSERT INTO `data_rows` VALUES (1,1,'id','number','ID',1,0,0,0,0,0,NULL,1),(2,1,'name','text','Name',1,1,1,1,1,1,NULL,2),(3,1,'email','text','Email',1,1,1,1,1,1,NULL,3),(4,1,'password','password','Password',1,0,0,1,1,0,NULL,4),(5,1,'remember_token','text','Remember Token',0,0,0,0,0,0,NULL,5),(6,1,'created_at','timestamp','Created At',0,1,1,0,0,0,NULL,6),(7,1,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,7),(8,1,'avatar','image','Avatar',0,1,1,1,1,1,NULL,8),(9,1,'user_belongsto_role_relationship','relationship','Role',0,1,1,1,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}',10),(10,1,'user_belongstomany_role_relationship','relationship','Roles',0,1,1,1,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}',11),(11,1,'settings','hidden','Settings',0,0,0,0,0,0,NULL,12),(12,2,'id','number','ID',1,0,0,0,0,0,NULL,1),(13,2,'name','text','Name',1,1,1,1,1,1,NULL,2),(14,2,'created_at','timestamp','Created At',0,0,0,0,0,0,NULL,3),(15,2,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,4),(16,3,'id','number','ID',1,0,0,0,0,0,NULL,1),(17,3,'name','text','Name',1,1,1,1,1,1,NULL,2),(18,3,'created_at','timestamp','Created At',0,0,0,0,0,0,NULL,3),(19,3,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,4),(20,3,'display_name','text','Display Name',1,1,1,1,1,1,NULL,5),(21,1,'role_id','text','Role',1,1,1,1,1,1,NULL,9),(22,4,'id','text','Id',1,0,0,0,0,0,'{}',1),(23,4,'name','text','Name',1,1,1,1,1,1,'{}',2),(24,4,'office_address','rich_text_box','Office Address',1,1,1,1,1,1,'{}',3),(25,4,'factory_address','rich_text_box','Factory Address',1,1,1,1,1,1,'{}',4),(26,4,'phone','text','Phone',1,1,1,1,1,1,'{}',5),(27,4,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',6),(28,4,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',7),(29,4,'deleted_at','timestamp','Deleted At',0,1,1,1,1,1,'{}',8),(30,5,'id','text','Id',1,1,0,0,0,0,'{}',1),(31,5,'name','text','Name',1,1,1,1,1,1,'{}',2),(32,5,'price','text','Price',1,1,1,1,1,1,'{}',3),(33,5,'created_at','timestamp','Created At',0,0,0,0,0,0,'{}',4),(34,5,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',5),(35,5,'deleted_at','timestamp','Deleted At',0,0,0,0,0,0,'{}',6),(36,6,'id','text','Id',1,0,0,0,0,0,'{}',1),(37,6,'name','text','Name',1,1,1,1,1,1,'{}',3),(38,6,'address','rich_text_box','Address',1,1,1,1,1,1,'{}',4),(39,6,'phone','text','Phone',1,1,1,1,1,1,'{}',5),(40,6,'product_id','text','Product',1,1,1,1,1,1,'{}',2),(41,6,'supply_capacity','text','Supply Capacity',1,1,1,1,1,1,'{}',6),(42,6,'created_at','timestamp','Created At',0,0,0,0,0,0,'{}',7),(43,6,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',8),(44,6,'deleted_at','timestamp','Deleted At',0,0,0,0,0,0,'{}',9),(45,7,'id','text','Id',1,1,1,1,1,1,'{}',1),(46,7,'product_id','select_dropdown','Product',1,1,1,1,1,1,'{}',2),(47,7,'buyer_id','select_dropdown','Buyer',1,1,1,1,1,1,'{}',3),(48,7,'producer_id','select_dropdown','Producer',1,1,1,1,1,1,'{}',4),(49,7,'created_at','timestamp','Created At',0,1,1,1,0,1,'{}',5),(50,7,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'{}',6),(51,7,'deleted_at','timestamp','Deleted At',0,1,1,1,1,1,'{}',7),(55,7,'order_belongstomany_buyer_relationship','relationship','buyers',0,1,1,1,1,1,'{\"model\":\"App\\\\Buyer\",\"table\":\"buyers\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"id\",\"pivot_table\":\"buyers\",\"pivot\":\"1\",\"taggable\":\"0\"}',8),(56,7,'order_belongstomany_producer_relationship','relationship','producers',0,1,1,1,1,1,'{\"model\":\"App\\\\Producer\",\"table\":\"producers\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"id\",\"pivot_table\":\"producers\",\"pivot\":\"1\",\"taggable\":\"0\"}',9),(57,7,'order_belongstomany_product_relationship','relationship','products',0,1,1,1,1,1,'{\"model\":\"App\\\\Product\",\"table\":\"products\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"id\",\"pivot_table\":\"products\",\"pivot\":\"1\",\"taggable\":\"0\"}',10),(58,6,'producer_hasmany_product_relationship','relationship','products',0,1,1,1,1,1,'{\"model\":\"App\\\\Product\",\"table\":\"products\",\"type\":\"hasMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"id\",\"pivot_table\":\"products\",\"pivot\":\"0\",\"taggable\":\"0\"}',10);
