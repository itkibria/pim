﻿# Host: localhost  (Version 5.7.24)
# Date: 2020-02-27 15:41:08
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "permissions"
#

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE `permissions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "permissions"
#

INSERT INTO `permissions` VALUES (1,'browse_admin',NULL,'2020-02-27 07:50:20','2020-02-27 07:50:20'),(2,'browse_bread',NULL,'2020-02-27 07:50:20','2020-02-27 07:50:20'),(3,'browse_database',NULL,'2020-02-27 07:50:20','2020-02-27 07:50:20'),(4,'browse_media',NULL,'2020-02-27 07:50:21','2020-02-27 07:50:21'),(5,'browse_compass',NULL,'2020-02-27 07:50:21','2020-02-27 07:50:21'),(6,'browse_menus','menus','2020-02-27 07:50:21','2020-02-27 07:50:21'),(7,'read_menus','menus','2020-02-27 07:50:21','2020-02-27 07:50:21'),(8,'edit_menus','menus','2020-02-27 07:50:21','2020-02-27 07:50:21'),(9,'add_menus','menus','2020-02-27 07:50:21','2020-02-27 07:50:21'),(10,'delete_menus','menus','2020-02-27 07:50:21','2020-02-27 07:50:21'),(11,'browse_roles','roles','2020-02-27 07:50:21','2020-02-27 07:50:21'),(12,'read_roles','roles','2020-02-27 07:50:21','2020-02-27 07:50:21'),(13,'edit_roles','roles','2020-02-27 07:50:21','2020-02-27 07:50:21'),(14,'add_roles','roles','2020-02-27 07:50:21','2020-02-27 07:50:21'),(15,'delete_roles','roles','2020-02-27 07:50:21','2020-02-27 07:50:21'),(16,'browse_users','users','2020-02-27 07:50:21','2020-02-27 07:50:21'),(17,'read_users','users','2020-02-27 07:50:21','2020-02-27 07:50:21'),(18,'edit_users','users','2020-02-27 07:50:22','2020-02-27 07:50:22'),(19,'add_users','users','2020-02-27 07:50:22','2020-02-27 07:50:22'),(20,'delete_users','users','2020-02-27 07:50:22','2020-02-27 07:50:22'),(21,'browse_settings','settings','2020-02-27 07:50:22','2020-02-27 07:50:22'),(22,'read_settings','settings','2020-02-27 07:50:22','2020-02-27 07:50:22'),(23,'edit_settings','settings','2020-02-27 07:50:22','2020-02-27 07:50:22'),(24,'add_settings','settings','2020-02-27 07:50:22','2020-02-27 07:50:22'),(25,'delete_settings','settings','2020-02-27 07:50:22','2020-02-27 07:50:22'),(26,'browse_buyers','buyers','2020-02-27 07:54:33','2020-02-27 07:54:33'),(27,'read_buyers','buyers','2020-02-27 07:54:33','2020-02-27 07:54:33'),(28,'edit_buyers','buyers','2020-02-27 07:54:33','2020-02-27 07:54:33'),(29,'add_buyers','buyers','2020-02-27 07:54:34','2020-02-27 07:54:34'),(30,'delete_buyers','buyers','2020-02-27 07:54:34','2020-02-27 07:54:34'),(31,'browse_products','products','2020-02-27 07:56:47','2020-02-27 07:56:47'),(32,'read_products','products','2020-02-27 07:56:47','2020-02-27 07:56:47'),(33,'edit_products','products','2020-02-27 07:56:47','2020-02-27 07:56:47'),(34,'add_products','products','2020-02-27 07:56:47','2020-02-27 07:56:47'),(35,'delete_products','products','2020-02-27 07:56:47','2020-02-27 07:56:47'),(36,'browse_producers','producers','2020-02-27 07:57:19','2020-02-27 07:57:19'),(37,'read_producers','producers','2020-02-27 07:57:19','2020-02-27 07:57:19'),(38,'edit_producers','producers','2020-02-27 07:57:19','2020-02-27 07:57:19'),(39,'add_producers','producers','2020-02-27 07:57:19','2020-02-27 07:57:19'),(40,'delete_producers','producers','2020-02-27 07:57:19','2020-02-27 07:57:19'),(41,'browse_orders','orders','2020-02-27 07:58:11','2020-02-27 07:58:11'),(42,'read_orders','orders','2020-02-27 07:58:11','2020-02-27 07:58:11'),(43,'edit_orders','orders','2020-02-27 07:58:11','2020-02-27 07:58:11'),(44,'add_orders','orders','2020-02-27 07:58:11','2020-02-27 07:58:11'),(45,'delete_orders','orders','2020-02-27 07:58:11','2020-02-27 07:58:11');
