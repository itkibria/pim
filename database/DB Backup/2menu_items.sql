﻿# Host: localhost  (Version 5.7.24)
# Date: 2020-02-27 15:39:07
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "menu_items"
#

DROP TABLE IF EXISTS `menu_items`;
CREATE TABLE `menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

#
# Data for table "menu_items"
#

INSERT INTO `menu_items` VALUES (1,1,'Dashboard','','_self','voyager-boat',NULL,NULL,1,'2020-02-27 07:50:19','2020-02-27 09:22:06','voyager.dashboard',NULL),(2,1,'Media','','_self','voyager-images',NULL,10,1,'2020-02-27 07:50:20','2020-02-27 09:26:41','voyager.media.index',NULL),(3,1,'Users','','_self','voyager-person',NULL,10,2,'2020-02-27 07:50:20','2020-02-27 09:26:41','voyager.users.index',NULL),(4,1,'Roles','','_self','voyager-lock',NULL,10,3,'2020-02-27 07:50:20','2020-02-27 09:26:41','voyager.roles.index',NULL),(5,1,'Tools','','_self','voyager-tools',NULL,NULL,7,'2020-02-27 07:50:20','2020-02-27 09:26:41',NULL,NULL),(6,1,'Menu Builder','','_self','voyager-list',NULL,5,1,'2020-02-27 07:50:20','2020-02-27 09:21:59','voyager.menus.index',NULL),(7,1,'Database','','_self','voyager-data',NULL,5,2,'2020-02-27 07:50:20','2020-02-27 09:21:59','voyager.database.index',NULL),(8,1,'Compass','','_self','voyager-compass',NULL,5,3,'2020-02-27 07:50:20','2020-02-27 09:21:59','voyager.compass.index',NULL),(9,1,'BREAD','','_self','voyager-bread',NULL,5,4,'2020-02-27 07:50:20','2020-02-27 09:21:59','voyager.bread.index',NULL),(10,1,'Settings','','_self','voyager-settings',NULL,NULL,6,'2020-02-27 07:50:20','2020-02-27 09:26:29','voyager.settings.index',NULL),(11,1,'Buyers','','_self','voyager-people','#000000',NULL,4,'2020-02-27 07:54:34','2020-02-27 09:37:33','voyager.buyers.index','null'),(12,1,'Products','','_self',NULL,NULL,NULL,5,'2020-02-27 07:56:47','2020-02-27 09:26:12','voyager.products.index',NULL),(13,1,'Producers','','_self','voyager-shop','#000000',NULL,3,'2020-02-27 07:57:19','2020-02-27 09:36:15','voyager.producers.index','null'),(14,1,'Orders','','_self','voyager-basket','#000000',NULL,2,'2020-02-27 07:58:11','2020-02-27 09:36:01','voyager.orders.index','null');
