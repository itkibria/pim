<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Kibria',
            'email' => 'akm.kibria@gmail.com',
            'password' => Hash::make('1q2w3e4r5t'),
            'role_id' => '1',
        ]);
    }
}
